package main

import (
    "github.com/minio/minio-go"
    "log"
    "strconv"
    "fmt"
    "os"
    "github.com/pborman/uuid"


)

func main() {
    endpoint := os.Args[1] + ":9000"
    accessKeyID := "fc60b435d4aa"
    secretAccessKey := "3b89d09a54a5"
    useSSL := false

    // Initialize minio client object.
    minioClient, err := minio.New(endpoint, accessKeyID, secretAccessKey, useSSL)
    if err != nil {
        log.Fatalln(err)
    }

    // Make a new bucket called mymusic.
    bucketName := "notmymusic"
/*
    location := "us-east-1"

    log.Printf("Start the make bucket")

    err = minioClient.MakeBucket(bucketName, location)
    if err != nil {
        // Check to see if we already own this bucket (which happens if you run this twice)
        exists, err := minioClient.BucketExists(bucketName)
        if err == nil && exists {
            log.Printf("We already own %s\n", bucketName)
        } else {
            log.Fatalln(err)
        }
    }
    log.Printf("Successfully created %s\n", bucketName)
*/
    // Upload the zip file
    objectName := "syslog.zip"
    filePath := "/tmp/syslog.zip"
    contentType := "application/zip"

    count := 0
    limit, err := strconv.Atoi(os.Args[2])
    for count <= limit {
    // Upload the zip file with FPutObject
    //n, err := minioClient.FPutObject(bucketName, fmt.Sprint(objectName,strconv.Itoa(count)), filePath, contentType)
    _, err := minioClient.FPutObject(bucketName, fmt.Sprint(objectName,strconv.Itoa(count),uuid.New()), filePath, contentType)
    if err != nil {
        log.Fatalln(err)
    }
    //log.Printf("Successfully uploaded %s of size %d\n", objectName, n)
    count++
    }
    log.Printf("Exit\n")

}
